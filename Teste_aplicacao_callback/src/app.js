var teste = 0;

function funcaoPrincipal(err) {
    if(err){console.log(err);} 
    else {           
    console.log("funcaoPrincipal: " + teste);
    funcaoCallback()
        .then(function() {
            teste++;
            console.log("Depois da Promise: " + teste);
            funcaoCallback() // Último a ser chamado.
            .catch(err => {
                console.log("catch: " + err);
            })
        })
        .catch(function (err) {
            console.log("err" + teste);       
        });
    }
}

function funcaoCallback() {    
    teste++;
    console.log("Dentro do Callback: " + teste);
    return new Promise(function (resolve, reject) {
        if (teste === 1) {
            teste++
            console.log("Dentro da Promise: " +teste);
            setTimeout(function () {
                resolve()
            }, 2000)               
          
        } else {
            console.log("Rejeitado: ");
            reject("Deu super errado aqui");
        }
    });
}

funcaoPrincipal()