import { expect } from 'chai';
import { sum, sub, mult, div } from '../src/calc.js';

describe('Teste Calculadora', () => {
    describe('Smoke Tests', () => {       
        it('should exist the method `sum`', () => {            
           expect(sum).to.exist;
           expect(sum).to.be.a('function');
        });
        it('should exist the method `sub`', () => {            
            expect(sub).to.exist;
            expect(sub).to.be.a('function');
         });
         it('should exist the method `mult`', () => {            
            expect(mult).to.exist;
            expect(mult).to.be.a('function');
         });
         it('should exist the method `div`', () => {            
            expect(div).to.exist;
            expect(div).to.be.a('function');
         });
    });

    describe('Sum test', () => {
      it('Verifica soma', () => {            
         expect(sum(2,2)).to.be.equal(4);
      });
   });
   describe('Sub test', () => {
      it('Verifica subtração', () => {            
         expect(sub(2,2)).to.be.equal(0);
      });
   });
   describe('Mult test', () => {
      it('Verifica multiplicação', () => {            
         expect(mult(2,3)).to.be.equal(6);
      });
   });
   describe('Div test', () => {
      it('Verifica divisão', () => {            
         expect(div(2,1)).to.be.equal(2);
         expect(div(2,0)).to.be.equal('Não é possível divisão por zero!');
      });
   });
});