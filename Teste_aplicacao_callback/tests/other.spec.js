// // Para testar: npm test
// // npm test -- --bail -> Para o teste no primeiro erro 
// // npm run test:tdd   -> Roda o código e fica assistindo a cada mudança

// var expect = require('chai').expect;

// describe('Main', function() {
//     describe('Method A', function() {
//         context('Case 1', function() {           
//             it.skip('Este teste não aparecerá e ficará pendente por causa do metodo SKIP', function() {           
//                 console.log("skip")
//             });   
//         });
  
//         context('Case 2', function() {            
//             it('Teste normal', function() {            
//                 console.log("normal")
//             });
//         });

//         // context('Case 3', function() {            
//         //     it.only('Teste only', function() {            
//         //         console.log("só executará este Teste por causa do metodo ONLY")
//         //     });
//         // });
//     });
// });

// /// HOOKS: before, beforeEach, after, afterEach
// describe('Using Chai', function() {
//     // Para não precisar declarar a variavel 'arr' em todos os testes 'it', 
//     // declaramos esta dentro do 'describe' e atribuimos seu valor antes de cada bloco (beforeEach)
//     var arr;
  
//     // roda uma vez, antes do bloco
//     before(function() {
//       // podemos iniciar uma conexão no banco
//       // ou criar um conjunto de dados
//     });
  
//     // roda uma vez, depois do bloco
//     after(function() {
//       // podemos fechar conexão do banco
//       // ou apagar esse conjunto de dados
//     });
  
//     // roda todas as vezes, antes de CADA bloco
//     beforeEach(function() {
//       arr = [1, 2, 3];
//     });
  
//     // roda todas as vezes, depois de CADA bloco
//     afterEach(function() {  
//     });
  
//     it('Testando tamanho do array', function() {
//       arr.push(4);
//       expect(arr).to.have.lengthOf(4);     
//     });
  
//     it('Não contém numero 3', function() {
//       arr.pop()
//       expect(arr).to.not.includes(3)
//     });
   
//     it('should be an array', function() {     
//       expect(arr).to.be.a('array');   
//     });  
// });

